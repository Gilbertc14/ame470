var url = require("url"),
	querystring = require("querystring");
	var MS = require("mongoskin");

	var Client = require('node-rest-client').Client;
	var client = new Client();

	var db = MS.db("mongodb://3.85.31.210:27017/rssApp");


var passport = require('passport');
var fs = require('fs');
var path = require('path'),
  express = require('express'),
  db = require('mongoskin').db('mongodb://3.85.31.210:27017/test');


var mongoose = require('mongoose');
var configDB = require('./passport/config/database.js');
mongoose.connect(configDB.url); // connect to our database

var app = express();
var secret = 'test' + new Date().getTime().toString()

var session = require('express-session');
app.use(require("cookie-parser")(secret));
var MongoStore = require('connect-mongo')(session);
app.use(session( {store: new MongoStore({
   url: 'mongodb://3.85.31.210/test',
   secret: secret
})}));
app.use(passport.initialize());
app.use(passport.session());
var flash = require('express-flash');
app.use( flash() );

var bodyParser = require("body-parser");
var methodOverride = require("method-override");

app.use(methodOverride());
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended:false
}));
require('./passport/config/passport')(passport); // pass passport for configuration
require('./passport/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport


app.use(express.static(path.join(__dirname, 'public')));


app.listen(8080);


app.get("/", function (req, res) {
      res.redirect("/index.html");
});




  });
});



app.get("/getAllMemberships", isLoggedIn, function (req, res) {
	var userID = req.user.local.email;
  db.collection('data').find({friends:userID}).toArray(function(err, items) {
    res.send(JSON.stringify(items));
  });
});



app.get("/getFeedData", function (req, res) {
  var url = req.query.url;

  client.get(url, function (data, response) {
      // parsed response body as js object
      res.send(data);
  });
});

app.get("/editFeed", isLoggedIn, function (req, res) {
  var id = req.query.id;
  var newList = req.query.newList.split(",");
  db.collection("data").findOne({id: id}, function(err, result){
    result.friends = newList;
    db.collection("data").save(result, function(e1,r1){
      db.collection('data').find({}).toArray(function(e2, items) {
        res.send(JSON.stringify(items));
      });
    });
  });
});


app.get("/getUserInfo", isLoggedIn, function (req, res) {
  var userID = req.user.local.email;
  db.collection('userid').findOne({userID:userID},function(e2, result) {
      res.send(JSON.stringify(result));
  });
});



app.get("/deleteFeed", isLoggedIn, function (req, res) {
  var id = req.query.id;
  db.collection("data").remove({id: id}, function(err0, result0){
      db.collection('data').find({}).toArray(function(e2, items) {
        res.send(JSON.stringify(items));
      });
  });
});

app.get("/addFriend", isLoggedIn, function (req, res) {
    var friend = req.query.email;
		var userID = req.user.local.email;

		db.collection('userid').findOne({userID:userID},function(e2, result) {
				if(result){
					var currFL = result.friends || [];
					if(currFL.indexOf(friend)<=0){
							result.friends.push(friend);
							db.collection("userid").save(result, function(e, r){
								db.collection('userid').findOne({userID:userID},function(e2, items) {
									res.send(JSON.stringify(items));
								});
							});
					}
				}
				else{
					var newUserID = {
			      userID: userID,
			      fn: "",
			      ln:  "",
			      thumb:  "",
			     	friends: [friend]
			    };
					db.collection("userid").insert(newUserID, function(e, r){
						db.collection('userid').find({}).toArray(function(e2, items) {
							res.send(JSON.stringify(items));
						});
					});
				}
		});
});


app.get("/getMessages", isLoggedIn, function (req, res) {
    var chID = req.query.chID;
    var limit = parseInt(req.query.limit || "10")
    var skip = parseInt(req.query.skip || "0")
    db.collection('messages').find({chID:chID}).skip(skip).limit(limit).sort({time:-1}).toArray(function(e2, items) {
      res.send(JSON.stringify(items));
    });
});

app.get("/addMessage", isLoggedIn, function (req, res) {
    var userID = req.user.local.email;
    var chID = req.query.chID;
    var text = req.query.text;
    var id = userID + chID + new Date().getTime();
    var ts = new Date().getTime();
    var newMessage = {
      userID: userID,
      text: text,
      id: id,
      time: ts,
      chID: chID
    };
    db.collection("messages").insert(newMessage, function(e, r){
      res.send("1");
    });
});

app.get("/addFeed", isLoggedIn, function (req, res) {
    var text = req.query.text;
    var newFeed = {
      id: req.user.local.email + "id" + new Date().getTime(),
      text: text,
      ts:  new Date().getTime(),
      done: false,
			userID: req.user.local.email
    };
    var cb = function(err0, result){
      var cb1 = function(err, items) {
        res.send(JSON.stringify(items));
      }
      db.collection('data').find({}).toArray(cb1);
    }
    db.collection("data").insert(newFeed, cb);
});



app.get("/pic", function (req, res) {
    var userID = req.query.userID;
		db.collection('userid').findOne({userID:userID},function(e2, result) {
				if(result){
          var img = new Buffer(result.thumb.replace(/^data:image\/png;base64,/, ''), 'base64');
           res.writeHead(200, {
             'Content-Type': 'image/png',
             'Content-Length': img.length
           });
           res.end(img);
        }
        else{
          res.end("");
        }
    });
});

app.get("/editUserid", isLoggedIn, function (req, res) {
    var friend = req.query.email;
		var userID = req.user.local.email;
    var fname = req.query.fname || null;
    var lname = req.query.lname || null;
    var thumb = req.query.thumb || null;

		db.collection('userid').findOne({userID:userID},function(e2, result) {
				if(result){
				if(fname)	result.fname = fname;
				if(lname)	result.lname = lname;
				if(thumb)	result.thumb = thumb;
							db.collection("userid").save(result, function(e, r){
								res.send("1");
							});
				}
				else{
					var newUserID = {
			      userID: userID,
			      fn: fname,
			      ln:  lname,
			     	friends: [],
            thumb: thumb
			    };
					db.collection("userid").insert(newUserID, function(e, r){
						res.send("1");
					});
				}
		});
});





// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.send('noauth');
}
